<?php

class CatFish {
	public 	$numberOfLegs = 'tidak ada',
			$foodClassification = 'omnivora',
			$respirationOrgan = 'insang' ;

	public  $canFly = false,
	 		$canSwim = true;

	function eat(){
	 	return  "eat";
	 	}
	function run(){
		 return "run";
		 }
	function swim(){
		 return "swim";
		 }
	function fly(){
		 return "fly";
		 }
	function cry(){
		 return "cry";
		 }
	}
		
class BettaFish{
	public 	$numberOfLegs = 'tidak ada',
			$foodClassification = 'karnivora',
			$respirationOrgan = 'insang' ;

	 public $canFly = false,
	 		$canSwim = true;

	function eat(){
	 	return "eat";
	 	}
	function run(){
		 	return "run";
		 }
	function swim(){
		 	return "swim";
		 }
	function fly(){
		 	return "fly";
		 }
	function cry(){
		 	return "cry";
		 }
	}

class Crocodile{
	public 	$numberOfLegs = '4',
			$foodClassification = 'karnivora',
			$respirationOrgan = 'paru-paru' ;

	 public $canFly = false,
	 		$canSwim = true;

	function eat(){
	 	return "eat";
	 	}
	function run(){
		 	return "run";
		 }
	function swim(){
		 	return "swim";
		 }
	function fly(){
		 	return "fly";
		 }
	function cry(){
		 	return "cry";
		 }
	}

class Aligator{
	public 	$numberOfLegs = '4',
			$foodClassification = 'karnivora',
			$respirationOrgan = 'paru-paru' ;

	 public $canFly = false,
	 		$canSwim = true;

	function eat(){
	 	return "eat";
	 	}
	function run(){
		 	return "run";
		 }
	function swim(){
		 	return "swim";
		 }
	function fly(){
		 	return "fly";
		 }
	function cry(){
		 	return "cry";
		 }
	}

class Lizard{
	public 	$numberOfLegs = '4',
			$foodClassification = 'omnivora',
			$respirationOrgan = 'paru-paru' ;

	 public $canFly = false,
	 		$canSwim = true;

	function eat(){
	 	return "eat";
	 	}
	function run(){
		 	return "run";
		 }
	function swim(){
		 	return "swim";
		 }
	function fly(){
		 	return "fly";
		 }
	function cry(){
		 	return "cry";
		 }
	}

class Snake{
	public 	$numberOfLegs = 'tidak ada',
			$foodClassification = 'karnivora',
			$respirationOrgan = 'paru-paru' ;

	 public $canFly = false,
	 		$canSwim = true;

	function eat(){
	 	return "eat";
	 	}
	function run(){
		 	return "run";
		 }
	function swim(){
		 	return "swim";
		 }
	function fly(){
		 	return "fly";
		 }
	function cry(){
		 	return "cry";
		 }
	}

class Swan {
	public 	$numberOfLegs = '4',
			$foodClassification = 'herbivora',
			$respirationOrgan = 'paru-paru' ;

	public  $canFly = true,
	 		$canSwim = true;

	function eat(){
	 	return "eat";
	 	}
	function run(){
		 	return "run";
		 }
	function swim(){
		 	return "swim";
		 }
	function fly(){
		 	return "fly";
		 }
	function cry(){
		 	return "cry";
		 }
	}

class Chicken{
	public 	$numberOfLegs = '2',
			$foodClassification = 'omnivora',
			$respirationOrgan = 'paru-paru' ;

	 public $canFly = true,
	 		$canSwim = false;

	function eat(){
	 	return "eat";
	 	}
	function run(){
		 	return "run";
		 }
	function swim(){
		 	return "swim";
		 }
	function fly(){
		 	return "fly";
		 }
	function cry(){
		 	return "cry";
		 }
	}

class Swallow{
	public 	$numberOfLegs = '2',
			$foodClassification = 'omnivora',
			$respirationOrgan = 'paru-paru


			' ;

	public  $canFly = true,
	 		$canSwim = false;

	function eat(){
	 	return  "eat";
	 	}
	function run(){
		 return "run";
		 }
	function swim(){
		 return "swim";
		 }
	function fly(){
		 return "fly";
		 }
	function cry(){
		 return "cry";
		 }
	}

class Duck{
	public 	$numberOfLegs = '2',
			$foodClassification = 'omnivora',
			$respirationOrgan = 'paru-paru' ;

	public  $canFly = true,
	 		$canSwim = true;

	function eat(){
	 	return  "eat";
	 	}
	function run(){
		 return "run";
		 }
	function swim(){
		 return "swim";
		 }
	function fly(){
		 return "fly";
		 }
	function cry(){
		 return "cry";
		 }
	}

class Human{
	public 	$numberOfLegs = '2',
			$foodClassification = 'omnivora',
			$respirationOrgan = 'paru-paru' ;

	public  $canFly = false,
	 		$canSwim = true;

	function eat(){
	 	return  "eat";
	 	}
	function run

	(){
		 return "run";
		 }
	function swim(){
		 return "swim";
		 }
	function fly(){
		 return "fly";
		 }
	function cry(){
		 return "cry";
		 }
	}

class Whale{
	public 	$numberOfLegs = 'tidak ada',
			$foodClassification = 'omnivora',
			$respirationOrgan = 'paru-paru' ;

	public  $canFly = false,
	 		$canSwim = true;

	function eat(){
	 	return  "eat";
	 	}
	function run(){
		 return "run";
		 }
	function swim(){
		 return "swim";
		 }
	function fly(){
		 return "fly";
		 }
	function cry(){
		 return "cry";
		 }
	}

class Dolphine{
	public 	$numberOfLegs = 'tidak ada',
			$foodClassification = 'omnivora',
			$respirationOrgan = 'paru-paru' ;

	public  $canFly = false,
	 		$canSwim = true;

	function eat(){
	 	return  "eat";
	 	}
	function run(){
		 return "run";
		 }
	function swim(){
		 return "swim";
		 }
	function fly(){
		 return "fly";
		 }
	function cry(){
		 return "cry";
		 }
		}

class Bat{
	public 	$numberOfLegs = '4',
			$foodClassification = 'omnivora',
			$respirationOrgan = 'paru-paru' ;

	public  $canFly = true,
	 		$canSwim = false;

	function eat(){
	 	return  "eat";
	 	}
	function run(){
		 return "run";
		 }
	function swim(){
		 return "swim";
		 }
	function fly(){
		 return "fly";
		 }
	function cry(){
		 return "cry";
		 }
		}

class Tiger{
	public 	$numberOfLegs = '4',
			$foodClassification = 'karnivora',
			$respirationOrgan = 'paru-paru' ;

	public  $canFly = false,
	 		$canSwim = true;

	function eat(){
	 	return  "eat";
	 	}
	function run(){
		 return "run";
		 }
	function swim(){
		 return "swim";
		 }
	function fly(){
		 return "fly";
		 }
	function cry(){
		 return "cry";
		 }
	}

class Persegi{
	public 	$panjang =  2.0,
			$lebar = 2.0;

	function hitung_keliling($panjang,$lebar)  
      {  
	      $keliling= 4*$panjang*$lebar;  
	      return $keliling;  
      }  

	function luas(){
		return "luas";
	  }
	}

class PersegiPanjang{
	public 	$panjang =10.0,
			$lebar = 6.0;

	function hitung_keliling($panjang,$lebar)  
      {  
	      $keliling= 2*($panjang + $lebar);  
	      return $keliling;  
      }  

	function luas(){
		return "luas";
	  }

	}

class Segitiga{
	public 	$a =5.0,
			$b = 5.0,
			$c = 5.0;

	function hitung_keliling($a,$b,$c)  
      {  
	      $keliling= $a*$b*$c;  
	      return $keliling;  
      }  

	function luas(){
		return "luas";
	  }

	}

class Lingkaran{
	public 	$r = 22.0,
			$phi = 3.14;

	function hitung_keliling(){
		$keliling= $phi*($r+$r);
		 return $keliling; 
	}

	function luas(){
		return "luas";
	 }
	}

class Trapesium{
	public 	$a = 10.0,
			$b = 12.0,
			$c = 8.0,
			$d = 9.0;

	function hitung_keliling($a,$b,$c,$d){
		$keliling= $a+$b+$c+$d;
		 return $keliling; 
	}

	function luas(){
		return "luas";
		}
	}

class JajarGenjang{
	public 	$panjang = 4.0,
			$lebar = 2.0;

	function hitung_keliling($panjang,$lebar){
		$keliling= (2*$panjang) + (2*$lebar) ;
		 return $keliling; 
	}

	function luas(){
		return "luas";
		}

	}

class BelahKetupat{
	public 	$panjang = 8.0,
			$lebar = 6.0;

	function hitung_keliling($panjang,$lebar){
		$keliling= 4*($panjang+$lebar);
		 return $keliling; 
	}

	function luas(){
		return "luas";
		}
	}